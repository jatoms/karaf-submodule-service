package ${JATOMS.package}.basic;

import ${JATOMS.package}.api.GreeterService;
import org.osgi.service.component.annotations.Component;

@Component
public class BasicGreeter implements GreeterService {
    
    @Override
    public String greet(String toGreet) {
        return "Hello " + toGreet;
    }
}