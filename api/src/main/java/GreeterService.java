package ${JATOMS.package}.api;

public interface GreeterService {
    public String greet(String toGreet);
}